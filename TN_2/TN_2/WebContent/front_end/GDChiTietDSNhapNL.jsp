<%@page import="dao.TKHoadonnhapDAO"%>
<%@page import="model.TKHoadonnhap"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
	String ngaybatdau = (String)request.getParameter("ngaybatdau");
	String ngayketthuc = (String)request.getParameter("ngayketthuc");
	int idncc = Integer.parseInt((String)request.getParameter("id_ncc"));
	String tenncc = (String)request.getParameter("ten_ncc");
	int tongtien = Integer.parseInt((String)request.getParameter("tongtien"));
	
	ArrayList<TKHoadonnhap> tk = (new TKHoadonnhapDAO()).getDanhSachHD(idncc, ngaybatdau, ngayketthuc);
%>
<%@include file="header.jsp" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GD chi tiết ds nguyên liệu</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
</head>
<body style="background-image: url('img/bg_dsnguyenlieu.jpeg');background-size: 100% 130%">
<div class="container" align="center" style="margin-top: 20px">
<div class="row">
	<h1 style="color: #fff">Danh Sách Các Lần Nhập Nguyên Liệu</h1>
</div>
<div class="row" style="font-size: 20px;font-style: italic;font-weight: 600;color: #1dd8e4">
	<div class="col-6" align="right">
		<p>Ngày bắt đầu là: <%= ngaybatdau %></p>
	</div>
	<div class="col-6" align="left">
		<p>Ngày kết thúc là: <%= ngayketthuc %></p>
	</div>

	<p>Tên nhà cung cấp là: <%= tenncc %></p>
	<p>Tổng tiền nhập: <%= tongtien %></p>
</div>
<div class="row">
	 <table class="table table-striped">
		<thead align="center" class="table-danger" style=" font-weight: bold">
			<td>STT</td>
			<td>Ngày nhập</td>
			<td>Tên Hóa Đơn</td>
			<td>Tổng mặt hàng nhập</td>
			<td>Tổng số tiền</td>
			<td>Chọn</td>
		</thead>
	<%
	if(tk != null){
		for(int i = 0; i < tk.size(); i++){
			%>
		<tbody align="center" class="table-primary">
			<td><%=i+1 %></td>
			<td><%=tk.get(i).getNgaygio() %></td>
			<td><%=tk.get(i).getTen() %></td>
			<td><%=tk.get(i).getTongsoluong() %></td>
			<td><%=tk.get(i).getTongtien() %></td>
			<td><form action="GDChiTietHoaDon.jsp">
					<input type="hidden" name="ten_hd" value="<%=tk.get(i).getTen() %>">
					<input type="hidden" name="ngaynhap" value="<%=tk.get(i).getNgaygio() %>">
					<input type="hidden" name="ten_ncc" value="<%=tenncc %>">
					<input type="hidden" name="tongtien" value="<%=tk.get(i).getTongtien() %>">
					<input class="btn btn-primary" type="submit" value="Xem chi tiết">
				</form>	
			</td>
		</tbody>
		<%}
	}
	%>	
	</table>
</div>


<button class="btn btn-success" style="margin: 15px" onclick="openPage('GDDanhSachNCC.jsp')">Quay Lại</button>
</div>
</body>
</html>