<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GD chọn loại TK NCC</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
<%@include file ="header.jsp" %>
</head>
<body style="background-image: url('img/bg_4k_3.jpg');background-size: 100% 200%">
	<div class="container" style="margin-top: 100px" align="center">
		<h1 style="color: #fff">Chọn Loại Thống Kê Nhà Cung Cấp</h1>
		<form action="">
			<input class="btn btn-success" style="margin: 15px" type="button" onclick="openPage('GDChonThoiGianThongKe.jsp')" value="Thống kê nhà cung cấp theo doanh chi">
			<br>
			<input class="btn btn-success" style="margin: 15px" type="button" value="Thống kê nhà cung cấp chất lượng">
			
		</form>
		<button class="btn btn-success" style="margin: 15px" onclick="openPage('GDChonLoaiTK.jsp')">Quay Lại</button>
	</div>
</body>
</html>