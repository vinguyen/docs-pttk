<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="header.jsp" %>
<title>GD chọn loại thống kê</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
</head>

<body style="background-image: url('img/bg_4k_3.jpg');background-size: 100% 170%">
	<div class="container" align="center" style="margin-top:100px"">
		<h1 style="color: #fff">Chọn Loại Thông Kê</h1>
		<form action="">
			<input class="btn btn-success" style="margin: 15px; type="button" onclick="openPage('GDChonLoaiTKNCC.jsp')" value="Thống kê nhà cung cấp">
			<br>
			<input class="btn btn-success" style="margin: 15px" type="button" value="Thống kê khách hàng">
			<br>
			<input class="btn btn-success" style="margin: 15px" type="button" value="Thống kê dịch vụ">
		</form>
		<button class="btn btn-success" style="margin: 15px" onclick="openPage('GDQuanLi.jsp')">Quay Lại</button>
	</div>
</body>
</html>