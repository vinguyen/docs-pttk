<%@page import="dao.CungcapDAO"%>
<%@page import="model.Cungcap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.TKHoadonnhapDAO"%>
<%@page import="model.Hoadonnhap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
    	String tenhd = (String)request.getParameter("ten_hd");
    	String ngaynhap = (String)request.getParameter("ngaynhap");
    	String tenncc = (String)request.getParameter("ten_ncc");
    	int tongtien = Integer.parseInt((String)request.getParameter("tongtien"));
    	ArrayList<Cungcap> cc = (new CungcapDAO()).getChiTietHD(tenhd);
    %>
<!DOCTYPE html>
<html>
<head>
<%@include file="header.jsp" %>
<meta charset="ISO-8859-1">
<title>GD chi tiết hóa đơn</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
</head>
<body style="background-image: url('img/bg_4k_1.jpg');background-size: 100%">
<div class="container" align="center" style="margin-top: 50px">
	<h1>Giao diện chi tiết hóa đơn</h1>
	<div class="row" style="font-weight: 500;color: #554343">
		<p>Tên nhà cung cấp: <%= tenncc %></p>
		<p>Ngày nhập: <%=ngaynhap %></p>
		<p>Tên hóa đơn: <%=tenhd %></p>
<p>Tổng tiền: <%=tongtien %></p>
	</div>
<table class="table table-striped">
	<thead align="center" style="font-weight: bold">
		<td>STT</td>
		<td>ID</td>
		<td>Tên nguyên liệu</td>
		<td>Đơn giá</td>
		<td>Số lượng</td>
		<td>Thành tiền</td>
	</thead>
<%
if(cc != null){
	for(int i = 0; i < cc.size(); i++){
		%>
		<tbody align="center" style="font-weight: 600">
			<td><%=i+1 %></td>
			<td><%=cc.get(i).getNl().getId() %></td>
			<td><%=cc.get(i).getNl().getTen() %></td>
			<td><%=cc.get(i).getNl().getDongia() %></td>
			<td><%=cc.get(i).getSoluong() %></td>
			<td><%=cc.get(i).getNl().getDongia()*cc.get(i).getSoluong() %></td>
		</tbody>
	<%}
}
%>	

</table>
<button class="btn btn-primary" style="margin: 30px" onclick="openPage('GDQuanLi.jsp')">Xác nhận</button>

<button class="btn btn-success" style="margin-left: 30px">Quay Lại</button>
</div>
</body>
</html>