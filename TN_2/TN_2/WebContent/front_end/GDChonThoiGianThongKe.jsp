<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GD chọn thời gian</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
<%@include file="header.jsp" %>
</head>
<body style="background-image: url('img/bg_time.jpg');background-size: 100% 235%">
<div class="container" align="center" style="margin-top: 100px">
	<h1>Chọn Thời Gian Xem Thống Kê</h1>
	<form action="GDDanhSachNCC.jsp" method="post">
		<table>
			<tr>
				<td>Ngày bắt đầu: </td>
				<td>
					<input type="date" name="ngaybatdau" value="2020-01-01">
				</td>
			</tr>
			<tr>
				<td>Ngày kết thúc: </td>
				<td>
					<input type="date" name="ngayketthuc" value="2020-12-30">
				</td>
			</tr>
		</table>
		<br>
		<input class="btn btn-primary" style="margin: 20px" type="submit" value="Xem thống kê" name="btnXemthongketheongay">
		<input class="btn btn-success" style="margin: 20px" type="button" onclick="openPage('GDChonLoaiTKNCC.jsp')" value="Quay Lại">
	</form>
</div>
</body>
</html>