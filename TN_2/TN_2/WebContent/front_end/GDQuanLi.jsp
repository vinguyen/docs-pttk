<%@page import="model.Nhanvien"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//lấy id của quản lí
	Nhanvien ql = (Nhanvien)session.getAttribute("quanli");
		if(ql == null){
			response.sendRedirect("doDangNhap.jsp?err=timeout");
		}
%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
<meta charset="ISO-8859-1">
<%@include file="header.jsp" %>
<title>GD Quản lí</title>
</head>
<body style="background-image: url('img/bg_4k_1.jpg');background-size: 100% 155%">
	<div class="container" align="center" style="margin-top: 100px">
		<h1>Trang Chủ Quản Lí</h1>
		<p style="font-size: 30px;color: #4f1e1e"><b><i>Quản lí: <%= ql.getHoten().getHo()%> <%= ql.getHoten().getTendem() %> <%= ql.getHoten().getTen() %> </i></b></p>
		<form action="">
		
			<p><input class="btn btn-success" style="margin: 5px" type="button" onclick="openPage('GDChonLoaiTK.jsp')" value="Xem thống kê">
			<p><input class="btn btn-success" style="margin: 5px" type="button" value="Quản lí nhân viên">
			<p><input class="btn btn-success" style="margin: 5px" type="button" value="Quản lí nguyên liệu">
			<p><input class="btn btn-success" style="margin: 5px" type="button" value="Đăng xuất" onclick="openPage('GDDangNhap.jsp')">
		</form>
	</div>

<div class="bg-image" style="background-image: url('img/bg_2.jpg')"></div>
</body>
</html>