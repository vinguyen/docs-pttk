<%@page import="dao.NhanvienDAO"%>
<%@page import="model.Nhanvien"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String username = (String)request.getParameter("username");
	String password = (String)request.getParameter("password");
	
	Nhanvien nv = new Nhanvien();
	nv.setUsername(username);
	nv.setPassword(password);
	
	NhanvienDAO dao = new NhanvienDAO();
	boolean kq = dao.kiemtraDangNhap(nv);
	
	if(kq && (nv.getVaitro().equalsIgnoreCase("quanli"))){
		session.setAttribute("quanli", nv);
		response.sendRedirect("GDQuanLi.jsp");
	}
	else {
		response.sendRedirect("GDDangNhap.jsp?err=fail");
	}
%>