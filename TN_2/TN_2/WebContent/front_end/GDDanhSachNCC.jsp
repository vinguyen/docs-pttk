<%@page import="dao.TKNccDAO"%>
<%@page import="model.TKNcc"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,java.text.SimpleDateFormat"%>
<%
	String ngaybatdau = (String)request.getParameter("ngaybatdau");
	String ngayketthuc = (String)request.getParameter("ngayketthuc");
	ArrayList<TKNcc> tk = (new TKNccDAO()).getDanhSachNcc(ngaybatdau, ngayketthuc);
	int S=0;
	session.setAttribute("tk",tk);

 %>
<!DOCTYPE html>
<html>
<head>
<%@include file="header.jsp" %>
<meta charset="ISO-8859-1">
<title>Danh sách nhà cung cấp</title>
<link rel="stylesheet" href="bootstrap/bootstrap.css">
</head>
<body style="background-image: url('img/bg_4k_3.jpg'); background-size: 100% 135%">
	<div class="container" align="center" style="margin-top: 50px">
	<div class="row">
		<h1>Danh Sách Nhà Cung Cấp</h1>
	</div>
	<div class="row">
		<p style="font-size: 20px;font-style: italic;font-weight: 600;color: #dc6e22">Ngày bắt đầu là: <%= ngaybatdau%> </p>
		<p style="font-size: 20px;font-style: italic;font-weight: 600;color: #dc6e22">Ngày kết thúc là: <%= ngayketthuc %></p>
	</div>	
	<div class="row">
		<%
		if(tk != null){%>
		<table class="table table-dark table-striped">
			<thead class="table-warning" style=" font-weight: bold">
				<td align="center">STT</td>
				<td align="center">Mã NCC</td>
				<td align="center">Tên NCC</td>
				<td align="center">Tổng số lượng nhập</td>
				<td align="center">Tổng tiền</td>
				<td align="center">Chọn</td>
			</thead>
		
			<%for(int i = 0; i < tk.size(); i++){
				S+=tk.get(i).getTongtien();
			%>
				<tbody align="center" class="table-secondary">
					<td><%= i+1 %></td>
					<td><%= tk.get(i).getId() %></td>
					<td><%= tk.get(i).getHoten().getHo()%> <%= tk.get(i).getHoten().getTendem()%> <%= tk.get(i).getHoten().getTen() %></td>
					<td><%= tk.get(i).getTongsoluong() %></td>
					<td><%= tk.get(i).getTongtien() %></td>
					<td><form action="GDChiTietDSNhapNL.jsp" method="post">
							<input type="hidden" name="id_ncc" value="<%=tk.get(i).getId()%>">
							<input type="hidden" name="ten_ncc" value="<%= tk.get(i).getHoten().getHo()%> <%= tk.get(i).getHoten().getTendem()%> <%= tk.get(i).getHoten().getTen() %>">
							<input type="hidden" name="tongtien" value="<%= tk.get(i).getTongtien() %>">
							<input type="hidden" name="ngaybatdau" value="<%=ngaybatdau%>">
							<input type="hidden" name="ngayketthuc" value="<%=ngayketthuc%>">
							<input class="btn btn-primary" type="submit" value="Xem chi tiết">
						</form>
					</td>
				</tbody>
				<%}%>
		</table>
			<h3>Tổng doanh chi: <p style="color: #1a3ab5"><%=S %></h3>	
		<%}
		else {%>
			<h2 style="color: red">Không có nhà cung cấp nào trong khoảng thời gian trên!</h2>
		<%}
		%>	
		
	</div>		
	</div>
	<div class="container-fluid" align="center">
		<input class="btn btn-success" type="button" onclick="openPage('GDChonThoiGianThongKe.jsp')" value="Quay lại" style="width: 200px">
	</div>
</body>
</html>