package dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import model.Hoten;
import model.Nhanvien;

public class NhanvienDAO extends DAO{
	public NhanvienDAO() {
		super();
	}
	
	public boolean kiemtraDangNhap(Nhanvien nv) {
		boolean kq = false;
		if(nv.getUsername().contains("true") || nv.getUsername().contains("=") 
				|| nv.getPassword().contains("true") || nv.getPassword().contains("="))
			return false;
		String sql = "{call kiemtraDN(?,?)}";
		try {
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, nv.getUsername());
			cs.setString(2, nv.getPassword());
			ResultSet rs = cs.executeQuery();
			
			if(rs.next()) {
				nv.setId(rs.getInt("idnhanvien"));
				nv.setVaitro(rs.getString("vaitro"));
				
				//lay ho ten
				Hoten ht = new Hoten();
				ht.setHo(rs.getString("ho"));
				ht.setTendem(rs.getString("tendem"));
				ht.setTen(rs.getString("ten"));
				
				nv.setHoten(ht);
				kq = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			kq =false;
		}
		return kq;
	}
}
