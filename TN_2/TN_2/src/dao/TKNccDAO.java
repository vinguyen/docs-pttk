package dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import model.Hoten;
import model.Ncc;
import model.TKNcc;

public class TKNccDAO extends DAO{
	public TKNccDAO() {
		super();
	}
	
	public ArrayList<TKNcc> getDanhSachNcc(String ngaybatdau, String ngayketthuc){
		ArrayList<TKNcc> kq = null;
		String sql = "{call layTkNcc(?,?)}";
		
		try {
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, ngaybatdau);
			cs.setString(2, ngayketthuc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				if(kq == null)
					kq = new ArrayList<TKNcc>();
				TKNcc tk= new TKNcc();
				Hoten ht = new Hoten();
//				Ncc ncc = new Ncc();
				ht.setHo(rs.getString("ho"));
				ht.setTendem(rs.getString("tendem"));
				ht.setTen(rs.getString("ten"));
				tk.setTongsoluong(rs.getInt("tongsoluong"));
				tk.setTongtien(rs.getInt("tongtien"));
				tk.setId(rs.getInt("idncc"));
				tk.setHoten(ht);
			
//				ncc.setId(rs.getInt("idncc"));
//				ncc.setHoten(ht);
//				tk.setNcc(ncc);
				kq.add(tk);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return kq;
	}
}
