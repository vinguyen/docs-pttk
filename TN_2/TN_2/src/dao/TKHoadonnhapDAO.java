package dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.Cungcap;
import model.Hoadonnhap;
import model.Nguyenlieu;
import model.TKHoadonnhap;

public class TKHoadonnhapDAO extends DAO{
	public TKHoadonnhapDAO() {
		super();
	}
	public ArrayList<TKHoadonnhap> getDanhSachHD(int idncc, String ngaybatdau, String ngayketthuc){
		ArrayList<TKHoadonnhap> kq = null;
		String sql = "{call layDSHD(?,?,?)}";
		try {
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, idncc);
			cs.setString(2, ngaybatdau);
			cs.setString(3, ngayketthuc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				if(kq == null) {
					kq = new ArrayList<TKHoadonnhap>();
				}
				TKHoadonnhap tk = new TKHoadonnhap();
				tk.setNgaygio(rs.getDate("ngaygio"));
				tk.setTongsoluong(rs.getInt("tongsoluong"));
				tk.setTongtien(rs.getInt("tongsotien"));
				tk.setTen(rs.getString("ten"));
				kq.add(tk);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		return kq;
		
	}
}