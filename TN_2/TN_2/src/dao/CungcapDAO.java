package dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.Cungcap;
import model.Nguyenlieu;

public class CungcapDAO extends DAO{
	public CungcapDAO() {
		super();
	}
	
	public ArrayList<Cungcap> getChiTietHD(String tenhd){
		ArrayList<Cungcap> kq = null;
		String sql = "{call layCTHD(?)}";
		try {
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, tenhd);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				if(kq == null) {
					kq = new ArrayList<Cungcap>();
				}
				Cungcap cc = new Cungcap();
				Nguyenlieu nl = new Nguyenlieu();
				
				nl.setId(rs.getInt("idnguyenlieu"));
				nl.setTen(rs.getString("ten"));
				nl.setDongia(rs.getInt("dongia"));
				cc.setSoluong(rs.getInt("soluong"));
				cc.setNl(nl);
				kq.add(cc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return kq;
	}
}
