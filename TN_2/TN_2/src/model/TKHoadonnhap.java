package model;

public class TKHoadonnhap extends Hoadonnhap{
	private int tongsoluong;
	private int tongtien;
	public int getTongsoluong() {
		return tongsoluong;
	}
	public void setTongsoluong(int tongsoluong) {
		this.tongsoluong = tongsoluong;
	}
	public int getTongtien() {
		return tongtien;
	}
	public void setTongtien(int tongtien) {
		this.tongtien = tongtien;
	}
	
}
