package model;

public class Diachi {
	private int id;
	private int sonha;
	private String toanha;
	private String duong;
	private String phuongxa;
	private String quanhuyen;
	private String tinhthanh;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSonha() {
		return sonha;
	}
	public void setSonha(int sonha) {
		this.sonha = sonha;
	}
	public String getToanha() {
		return toanha;
	}
	public void setToanha(String toanha) {
		this.toanha = toanha;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getPhuongxa() {
		return phuongxa;
	}
	public void setPhuongxa(String phuongxa) {
		this.phuongxa = phuongxa;
	}
	public String getQuanhuyen() {
		return quanhuyen;
	}
	public void setQuanhuyen(String quanhuyen) {
		this.quanhuyen = quanhuyen;
	}
	public String getTinhthanh() {
		return tinhthanh;
	}
	public void setTinhthanh(String tinhthanh) {
		this.tinhthanh = tinhthanh;
	}
	
}
