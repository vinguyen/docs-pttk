package model;



public class TKNcc extends Ncc{

	private int tongsoluong;
	private int tongtien;
	private String mota;
	private String chatluong;
	public int getTongsoluong() {
		return tongsoluong;
	}
	public void setTongsoluong(int tongsoluong) {
		this.tongsoluong = tongsoluong;
	}
	public int getTongtien() {
		return tongtien;
	}
	public void setTongtien(int tongtien) {
		this.tongtien = tongtien;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	public String getChatluong() {
		return chatluong;
	}
	public void setChatluong(String chatluong) {
		this.chatluong = chatluong;
	}
	
	
}
