package model;


import java.util.Date;

public class Hoadonnhap {
	private int id;
	private Date ngaygio;
	private String ten;
	private String mota;
	private Cungcap cc;
	private Nvkho nvkho;
	private Ncc ncc;
	
	public Cungcap getCc() {
		return cc;
	}
	public void setCc(Cungcap cc) {
		this.cc = cc;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getNgaygio() {
		return ngaygio;
	}
	public void setNgaygio(Date ngaygio) {
		this.ngaygio = ngaygio;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	
	public Nvkho getNvkho() {
		return nvkho;
	}
	public void setNvkho(Nvkho nvkho) {
		this.nvkho = nvkho;
	}
	public Ncc getNcc() {
		return ncc;
	}
	public void setNcc(Ncc ncc) {
		this.ncc = ncc;
	}
	
}
