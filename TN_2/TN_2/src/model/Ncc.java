package model;

public class Ncc {
	private int id;
	private Hoten hoten;
	private Diachi diachi;
	private String sdt;
	private String email;
	private String loaiNCC;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Hoten getHoten() {
		return hoten;
	}
	public void setHoten(Hoten hoten) {
		this.hoten = hoten;
	}
	public Diachi getDiachi() {
		return diachi;
	}
	public void setDiachi(Diachi diachi) {
		this.diachi = diachi;
	}
	public String getSdt() {
		return sdt;
	}
	public void setSdt(String sdt) {
		this.sdt = sdt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLoaiNCC() {
		return loaiNCC;
	}
	public void setLoaiNCC(String loaiNCC) {
		this.loaiNCC = loaiNCC;
	}
	
}
