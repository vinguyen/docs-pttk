package model;

import java.util.Date;

public class Nhanvien {
	private int id;
	private Hoten hoten;
	private Diachi diachi;
	private String username;
	private String password;
	private Date ngaysinh;
	private String sdt;
	private String email;
	private String vaitro;
	private String mota;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Hoten getHoten() {
		return hoten;
	}
	public void setHoten(Hoten hoten) {
		this.hoten = hoten;
	}
	public Diachi getDiachi() {
		return diachi;
	}
	public void setDiachi(Diachi diachi) {
		this.diachi = diachi;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getNgaysinh() {
		return ngaysinh;
	}
	public void setNgaysinh(Date ngaysinh) {
		this.ngaysinh = ngaysinh;
	}
	public String getSdt() {
		return sdt;
	}
	public void setSdt(String sdt) {
		this.sdt = sdt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVaitro() {
		return vaitro;
	}
	public void setVaitro(String vaitro) {
		this.vaitro = vaitro;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	
	
}
