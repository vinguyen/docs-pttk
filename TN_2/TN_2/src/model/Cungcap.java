package model;

public class Cungcap {
	private int id;
	private int soluong;
	private String mota;
	private Nguyenlieu nl;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSoluong() {
		return soluong;
	}
	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	public Nguyenlieu getNl() {
		return nl;
	}
	public void setNl(Nguyenlieu nl) {
		this.nl = nl;
	}
	
}
